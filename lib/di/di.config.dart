// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:samim/data/dataSource/countryDataSource.dart' as _i3;
import 'package:samim/data/dataSource/loginDataSorce.dart' as _i7;
import 'package:samim/data/repository/countryRepository.dart' as _i5;
import 'package:samim/data/repository/loginRepository.dart' as _i8;
import 'package:samim/domain/CountryUseCase.dart' as _i6;
import 'package:samim/domain/LoginUseCase.dart' as _i9;

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt init(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  gh.lazySingleton<_i3.ICountryDataSource>(
      () => _i3.CountryDataSource(gh<_i4.Dio>()));
  gh.lazySingleton<_i5.ICountryRepository>(
      () => _i5.CountryRepository(gh<_i3.ICountryDataSource>()));
  gh.lazySingleton<_i6.ICountryUseCase>(
      () => _i6.CountryUseCase(gh<_i5.ICountryRepository>()));
  gh.lazySingleton<_i7.ILoginDataSource>(() => _i7.LoginDataSource());
  gh.lazySingleton<_i8.ILoginRepository>(
      () => _i8.LoginRepository(gh<_i7.ILoginDataSource>()));
  gh.lazySingleton<_i9.ILoginUseCase>(
      () => _i9.LoginUseCase(gh<_i8.ILoginRepository>()));
  return getIt;
}
