import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';


abstract class ILoginDataSource {

  Future<bool> checkUserPassword(String userName,String password);
}

@LazySingleton(as: ILoginDataSource)
class LoginDataSource implements ILoginDataSource {
  @override
  Future<bool> checkUserPassword(String userName, String passwrod) async {
    List<dynamic> list = await readJson(userName, passwrod);
    for (var element in list) {
      print(element);
      if (element['email'] == userName &&
          element['state'] == "enable") {
        return true;
      }
    }
    return false;
  }

  Future<List<dynamic>> readJson(String userName, String passwrod) async {
    final String response = await rootBundle.loadString(
        'assets/files/users.json');
    return json.decode(response);
  }
}


