import 'package:dio/src/response.dart';
import 'package:injectable/injectable.dart';

import '../dataSource/countryDataSource.dart';

abstract class ICountryRepository implements ICountryDataSource {

}

@LazySingleton(as: ICountryRepository)
class CountryRepository implements ICountryRepository{
  final ICountryDataSource countryDataSource;
  CountryRepository(this.countryDataSource);

  @override
  Future<List<dynamic>> getCountries() => countryDataSource.getCountries();
}