import 'dart:developer';

import 'package:equatable/equatable.dart';

class Country extends Equatable{
  final String name;
  final String capital;
  final String  flagUrl;
  const Country(this.name,this.capital,this.flagUrl);
  Country.fromJson(Map<String,dynamic> json): name=json['name'],capital=json['capital'],flagUrl=json['flag'];
  @override
  List<Object?> get props =>[name,capital,flagUrl];

}