import 'package:injectable/injectable.dart';
import '../data/repository/loginRepository.dart';



abstract class ILoginUseCase implements ILoginRepository {
}
@LazySingleton(as: ILoginUseCase)
class LoginUseCase implements ILoginUseCase{
  final ILoginRepository repository;
  LoginUseCase(this.repository);

  @override
  Future<bool> checkUserPassword(String userName, String password) =>repository.checkUserPassword(userName,password);

}