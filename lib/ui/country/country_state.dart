part of 'country_bloc.dart';

@immutable
abstract class CountryState extends Equatable {
  List<Object?> get props => [];

}

// class CountryInitialState extends CountryState {}
class CountryLoadingState extends CountryState {}
class CountryEmptyState extends CountryState{
  final String message;
  CountryEmptyState(this.message);
  @override
  List<Object> get props =>[message];
}
class CountryErrorState extends CountryState {
  final AppException appException;
  CountryErrorState(this.appException);
  @override
  List<Object> get props => [appException];

}
class CountrySuccessState extends CountryState {
  List<Country> items;
  CountrySuccessState(this.items);
}
