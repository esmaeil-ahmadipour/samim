import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:samim/common/breakpoints.dart';
import '../../di/di.dart';
import '../../domain/CountryUseCase.dart';
import '../widget/emptyState.dart';
import '../widget/error.dart';
import 'country_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


class CountryScreen extends StatefulWidget {

  const CountryScreen( {Key? key, required this.togleTheme, required this.slectedLanguageChanged}) : super(key: key);
  final Function() togleTheme;
  final Function(language newLanguage) slectedLanguageChanged;

  @override
  State<CountryScreen> createState() => _CountryScreenState();
}
enum language { fa, en }

class _CountryScreenState extends State<CountryScreen> {
  language mylanguage = language.en;

  void _updateSelectedLanguage(language value) {
    widget.slectedLanguageChanged(value);
    setState(() {
      mylanguage = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    final localization=AppLocalizations.of(context)!;
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        appBar: AppBar(
          actions: [
            InkWell(
              onTap: widget.togleTheme,
              child: const Padding(
                padding: EdgeInsets.fromLTRB(32,8, 8, 32),
                child: Icon(CupertinoIcons.sun_dust_fill),
              ),
            )
          ],
          centerTitle: false,
          title: Padding(
          padding: const EdgeInsets.all(8.0),
          child:
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(localization.selectedLanguage),
              CupertinoSlidingSegmentedControl<language>(
                  thumbColor: Theme.of(context).colorScheme.primary,
                  groupValue: mylanguage,
                  children: {
                    language.en: Text(
                      localization.enLanguage,
                      style: const TextStyle(fontSize: 14),
                    ),
                    language.fa: Text(
                      localization.faLanguage,
                      style: const TextStyle(fontSize: 14),
                    )
                  },
                  onValueChanged: (value) =>
                      _updateSelectedLanguage(value!))
            ],
          ),
          // Row(
          //   children: [
          //      Text(localization.countyList,
          //         style: Theme.of(context).textTheme.titleMedium),
          //     Text(MediaQuery.of(context).displayName,
          //         style:Theme.of(context).textTheme.titleMedium),
          //   ],
          // ),
        ),),
        body: SafeArea(
            child: BlocProvider<CountryBloc>(
              create: (context) {
                final countryBlock = CountryBloc(getIt<ICountryUseCase>());
                countryBlock.add(CountryStarted());
                return countryBlock;
              },
              child: BlocBuilder<CountryBloc, CountryState>(
                  builder: (context, state) {
                    if (state is CountrySuccessState) {
                      final counrties = state.items;
                      return LayoutBuilder(
                          builder: (context, orientation) {
                            final int crossAxisCount;
                            switch(MediaQuery.of(context).displaySize){
                              case DisplaySize.extraSmall:
                                crossAxisCount= 1;
                                break;
                              case DisplaySize.small:
                                crossAxisCount= 2;
                                break;
                              case DisplaySize.medium:
                                crossAxisCount=3;
                                break;
                              case DisplaySize.large:
                                crossAxisCount= 4;
                            }
                       int count= crossAxisCount;
                      return  Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 16,left: 16,right: 16,bottom: 8),
                            child: SizedBox(
                              height: 80,
                              child: GridView.builder(
                                shrinkWrap: true,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: count,
                                  crossAxisSpacing: 8,
                                  mainAxisSpacing: 8,
                                  childAspectRatio:5),
                                itemCount:count,
                                itemBuilder: (BuildContext context, int index) {
                                return
                                    Container(
                                          color:Theme.of(context).backgroundColor,
                                          child: Center(
                                          child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children:  [
                                          Expanded(child: Center(child: Text(localization.name, style: Theme.of(context).textTheme.subtitle1))),

                                          Expanded(child: Center(child: Text(localization.capital, style: Theme.of(context).textTheme.subtitle1))),

                                          Expanded(child: Center(child: Text(localization.flag, style: Theme.of(context).textTheme.subtitle1)))
                                          ],
                                          ),
                                ),
                              );
                               },
                               ),
                         ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 16,left: 16,bottom: 16),

                              child: GridView.builder(
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                physics: const NeverScrollableScrollPhysics(),
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: count,
                                    crossAxisSpacing: 8,
                                    mainAxisSpacing: 8,
                                    childAspectRatio:5

                              ),
                                itemCount: counrties.length,

                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    color: Colors.yellow,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Expanded(child: Center(child: Text(counrties[index].name,style:Theme.of(context).textTheme.bodyText1))),

                                        Expanded(child: Center(child: Text(counrties[index].capital,style:Theme.of(context).textTheme.bodyText1))),

                                        Expanded(child: Center(child: SvgPicture.network(counrties[index].flagUrl,width: 50,)))
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        ],
                      );
                          }
                          );
                    }
                    else if (state is CountryLoadingState){
                      return const Center(child: CircularProgressIndicator());
                    }
                    else if(state is CountryErrorState) {
                      return AppErrorWidget(onPressed: () {
                        // context.read<ICountryUseCase>().add(CountryStarted());
                        CountryBloc(getIt<ICountryUseCase>()).add(CountryStarted());
                      },appException: state.appException);
                    }
                    else if(state is CountryErrorState){
                      return EmptyView(image: Image.asset('assets/img/empty_cart.svg',width: 140),message: state.appException.message,);
                    }
                    else{
                      throw Exception(localization.emptyCountry);
                    }
                  }
              ),
            )));

  }
}
