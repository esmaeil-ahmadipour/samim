
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../common/exception.dart';
import '../../domain/LoginUseCase.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {

  final ILoginUseCase useCase;

  LoginBloc(this.useCase) : super(const LoginInitial()) {
    on<LoginEvent>((event, emit)async {
      try{

     if(event is LoginBtnClicked)
       {
         emit(const LoginLoading());
         Future.delayed(const Duration(seconds: 3));
         final response=await useCase.checkUserPassword(event.userName, event.passWord);
         if(response) {
           emit(const LoginSuccess());
         }else{
           emit(LoginErorr(AppException(message: ' کاربر اجازه ورود ندارد')));
         }
       }
    }
    catch(e){
      emit(LoginErorr( AppException()));
    }
    });
  }
}
