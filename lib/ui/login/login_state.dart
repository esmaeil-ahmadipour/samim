part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();
  List<Object> get props => [];

}

class LoginInitial extends LoginState {
  const LoginInitial() : super();
}
class LoginLoading extends LoginState{
  const LoginLoading();
}

class LoginSuccess extends LoginState{
  const LoginSuccess();
}

class LoginErorr extends LoginState {
  final AppException appException;
  const LoginErorr( this.appException);
}
