
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import '../../common/routes.dart';
import '../../di/di.dart';
import '../../domain/LoginUseCase.dart';
import 'login_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<LoginScreen> {
  StreamSubscription? _stateStreamSubscription;

  @override
  void dispose() {
    super.dispose();
    _stateStreamSubscription?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    final localization=AppLocalizations.of(context)!;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(localization.login),
      ),
      body: BlocProvider<LoginBloc>(
        create: (context) {
          final bloc = LoginBloc(getIt<ILoginUseCase>());
          _stateStreamSubscription = bloc.stream.listen((state) {
            if (state is LoginSuccess) {
              Get.offNamed(Routes.countryScreen);
            }
            else if (state is LoginErorr) {
              showToast(state.appException.message);
              }
          });
          bloc.add(const LoginStarted());
          return bloc;
        },
        child: const SingleChildScrollView(
            child: SignUpForm()
        ),
      ),
    );
  }

}
void showToast(String message) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.yellow
  );}
  class SignUpForm extends StatefulWidget {
    const SignUpForm({Key? key}) : super(key: key);

    @override
    _SignUpFormState createState() => _SignUpFormState();

  }


  class _SignUpFormState extends State<SignUpForm> {

  final _formKey = GlobalKey<FormState>();
  bool isLoginEnabled=false;

  String? _password;
  String? _email;

  @override
  Widget build(BuildContext context) {
    final localization=AppLocalizations.of(context)!;
  validateEmail(String? value) {
  if (value!.isEmpty) {
  return localization.enterUserName;
  }

  Pattern pattern =
  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = RegExp(pattern.toString());
  if (!regex.hasMatch(value.toString())) {
  return localization.invalidUser;
  } else {
  return null;
  }
  }

  validatePassword(String? value)
  {
      if (value!.isEmpty) {
        return localization.enterUserName;
      }
      else if (value.length < 8) {
        return localization.moreThan8Character;
      } else {
        bool hasUppercase = value.contains(RegExp(r'[A-Z]'));
        if (!hasUppercase) {
          return localization.includeUperCase;
        }
        else {
          bool hasLowercase = value.contains(RegExp(
              r'[a-z]'));
          if (!hasLowercase) {
            return localization.includelowerCase;
          }
          else {
              final  alphaExp =RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');
              if(alphaExp.hasMatch(value.toString())) return"فقط حروف انگیلیسی وارد شود.";
              return null;
          }
        }
      }
    }

  void onPressedSubmit() {
  if (_formKey.currentState!.validate()) {
  _formKey.currentState?.save();
    BlocProvider.of<LoginBloc>(context)
        .add(LoginBtnClicked(
        _email!,
        _password!));
  }
  }

  return Form(
  key: _formKey,
  onChanged: () => setState(() => isLoginEnabled = _formKey.currentState!.validate() ),

      child: Padding(
  padding: const EdgeInsets.all(32.0),
  child: Column(
  children: [
  const SizedBox(height: 10,),
  TextFormField(
  decoration:
   InputDecoration(labelText: localization.enterUserName,
  hintText: localization.email,

  ),
  keyboardType: TextInputType.emailAddress,
  validator: validateEmail,
    onSaved: (value) {
    setState(() {
    _email = value.toString();
    });
    }),
    const SizedBox(height: 10,),
  TextFormField(
  obscureText: false,
  decoration:  InputDecoration(
  hintText: localization.password, labelText: localization.enterPassword),
  validator:validatePassword,
    onSaved: (value) {
      _password=value;
    },
 ),
  const SizedBox(height: 8,),
  BlocBuilder<LoginBloc, LoginState>(
  builder: (context, state) {
  return ElevatedButton(
  onPressed:isLoginEnabled ? onPressedSubmit : null,
  child: Padding(
  padding: const EdgeInsets.all(8.0),
  child: state is LoginLoading ? const CupertinoActivityIndicator() :  Text(localization.login)
  ));
  },
  )
  ]),));
  }
  }

