import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class LoadingImageService extends StatelessWidget {
  final String imageUrl;
  final BorderRadius? borderRadius;
  const LoadingImageService({
    Key? key,
    required this.imageUrl,
    required this.borderRadius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (borderRadius != null) {
      return ClipRRect(
        borderRadius: borderRadius,
        child: CachedNetworkImage(
          imageUrl: imageUrl,
          fit: BoxFit.cover,
        ),
      );
    }
    return CachedNetworkImage(
      imageUrl: imageUrl,
      fit: BoxFit.cover,
    );
  }
}
