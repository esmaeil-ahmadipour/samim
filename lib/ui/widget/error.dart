import 'package:flutter/material.dart';

import '../../common/exception.dart';

class AppErrorWidget extends StatelessWidget {
  final AppException appException;
  final GestureTapCallback onPressed;
  const AppErrorWidget(
      {Key? key, required this.appException, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(appException.message),
          ElevatedButton(
              onPressed: () {
                onPressed();
              },
              child: const Text('تلاش مجدد:'))
        ],
      ),
    );
  }
}
